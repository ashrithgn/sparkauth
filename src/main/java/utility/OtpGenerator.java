package utility;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Created by costrategix on 10/13/16.
 */
public class OtpGenerator {
    protected OtpGenerator() {

    }

    public static String random(int size) {

        StringBuilder generatedToken = new StringBuilder();
        try {
            SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
            // Generate 20 integers 0..20
            for (int i = 0; i < size; i++) {

                generatedToken.append(number.nextInt(9));
            }
            if (generatedToken.length() != size) {
                generatedToken.append(number.nextInt(9));
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "1234";//generatedToken.toString();
    }
}
