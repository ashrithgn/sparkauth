package utility;

import Framework.Conf.FrameworkConfiguration;
import Framework.Exception.Http.MissingRequiredParameter;
import Framework.Exception.Http.ValidationError;
import Framework.ORM.Adapter.interfaces.AdapterImplementation;
import org.apache.commons.httpclient.auth.*;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ashrith on 25/1/17.
 */
public class Validation {
    public static FrameworkConfiguration conf;
    public static AdapterImplementation dao;

    public Validation(FrameworkConfiguration conf,AdapterImplementation dao) {
        this.conf = conf;
        this.dao = dao;
    }

    public  void requiredKeyValidations(Map<String,Object> inParams, String[] keys) throws Exception{
        for (String key:keys) {
            if(inParams.getOrDefault(key,null) == null){
                throw new MissingRequiredParameter("The " + key + " paramater is required", "330");
            }
        }
    }

    public void passwordRulesValidation(String value) throws Exception {
        int minLength = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("passwordminlength")).get("value").toString());
        int maxLenght = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("passwordmaxlength")).get("value").toString());
        int isStrictRegex = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("strictpasswordflag")).get("value").toString());
        if(value.length()< minLength){
            throw new ValidationError("The password should contain minimum of " + minLength + " characters", "312");

        }

        if(value.length() > maxLenght){
            throw new ValidationError("The password should be less than " + maxLenght + " characters", "313");
        }

        if(isStrictRegex == 1){
            String regx = ((Map<String,Object>)conf.getSystemConfiguration().get("passwordRegex")).get("value").toString();
            Pattern patternObj = Pattern.compile(regx);
            Matcher matcherObj = patternObj.matcher(value);
            if(!matcherObj.matches()){
                throw new ValidationError("The password does not match the required pattern", "314");
            }

        }
    }

    public void emailValidations(String value) throws Exception {
        String emailRegex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        Pattern patternObj = Pattern.compile(emailRegex, Pattern.CASE_INSENSITIVE);
        Matcher matcherObj = patternObj.matcher(value);
        if (!matcherObj.find()) {
            throw new ValidationError("The email address is not valid", "315");
        }
    }

    public void mobileValidations(String value) throws Exception {
        String mobileRegex = "^\\+[1-9]{1}[0-9]{3,14}$";
        Pattern patternObj = Pattern.compile(mobileRegex);
        Matcher matcherObj = patternObj.matcher(value);
        if (!matcherObj.find()) {
            throw new ValidationError("The mobile number is not valid", "317");
        }

    }


}
