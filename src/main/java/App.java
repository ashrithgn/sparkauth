import Controller.UserController;
import Framework.Exception.ConfigurationException;
import Framework.Exception.DaoException;
import Framework.Exception.Http.*;
import Framework.Exception.JsonreaderException;
import Framework.Exception.ValidationException;
import Framework.Init;
import Framework.ORM.Adapter.interfaces.AdapterImplementation;
import Framework.ORM.OutputProcessor;
import Middleware.AuthMiddleware;

import static spark.Spark.*;
import static spark.Spark.exception;

/**
 * Created by ashrith on 24/1/17.
 */
public class App {
    public static AdapterImplementation dataAdapter;
    public static OutputProcessor processor;
    public static Framework.Init bootstrap = null;
    public static void main(String[] args) throws DaoException, ConfigurationException, JsonreaderException {
        if(args.length > 0 && !args[0].isEmpty()){
            bootstrap = new Framework.Init(args[0]);
        }else{
            bootstrap = new Framework.Init(System.getProperty("user.dir") + "/conf/applicationConfig.json");
        }
        dataAdapter = bootstrap.getDao();
        processor = bootstrap.getOutputProcessor();
        sparkConfig(bootstrap);
        Controllers();
        exceptions();

    }

    private static void Controllers() {
        UserController uc = new UserController(bootstrap);
        AuthMiddleware mc = new AuthMiddleware(bootstrap);

        options("*",((request, response) -> {

            return "OK";
        }));

        post("/auth/register",((request, response) -> {
            return uc.register(request,response);
        }));

        post("/auth/login",((request, response) -> {
            return uc.login(request,response);
        }));

        post("/auth/login/step-two",((request, response) -> {
            mc.multiFactorTokenCheck(request);
            return uc.steptwologin(request,response);
        }));

        get("/auth/activate/mobile",((request, response) -> {
            return uc.activateMobile(request,response);
        }));

        get("/auth/activate/email",((request, response) -> {
            return uc.activateEmail(request,response);
        }));

        get("/auth/user/password/reset", (request, response) -> {
            return uc.requestPasswordResetToken(request, response);
        });

        put("/auth/user/password/reset", (request, response) -> {
            return uc.resetPassword(request, response);
        });

        get("/auth/user", ((request, response) -> {
            mc.authTokenCheck(request);
            return uc.getlogedinuser(request, response);
        }));

        get("/auth/user/:loginid", ((request, response) -> {
            mc.authTokenCheck(request);
            return uc.getUserByloginId(request, response);
        }));

        get("auth/user/token/status", ((request, response) -> {
            mc.authTokenCheck(request);
            return uc.tokenCheck(request, response);
        }));

        get("auth/user/token/renew", ((request, response) -> {
            mc.authTokenCheck(request);
            return uc.renewToken(request, response);
        }));

        awaitInitialization();
    }

    private static void routes() {

    }

    private static void sparkConfig(Init bootstrap) {
        port(Integer.parseInt(bootstrap.config.getPort()));

        notFound((req, res) -> {
            res.type("application/json");
            res.status(404);
            return "{\"message\":\"page not found\",\"status\":\"0\",\"errorcode\":\"404\"}";
        });

        internalServerError((req, res) -> {
            res.type("application/json");
            res.status(500);
            return "{\"message\":\"internal server error\",\"status\":\"0\",\"errorcode\":\"500\"}";
        });

        before((request, response) -> {
            String header = request.contentType();
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            response.header("Access-Control-Allow-Origin", "*");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            if (request.requestMethod().equals("POST") || request.requestMethod().equals("PUT")) {
                if ((header == null) || (!header.equals("application/json"))) {
                    throw new AuthException("Content-Type must be application/json", "29");
                }
            }
        });


        after((request, response) -> {
            response.type("application/json");
            if(!request.requestMethod().equals("OPTIONS")){
                bootstrap.sparkLogWriter(request,response);
            }

        });


    }

    private static void exceptions() {
        exception(EmptyPostRequest.class, (exception, req, res) -> ExceptionHandler.emptyRequest(exception, req, res));
        exception(MissingRequiredParameter.class,(exception, req, res)-> ExceptionHandler.requiresParam(exception,req,res));
        exception(ValidationError.class,(exception, req, res)-> ExceptionHandler.validationError(exception,req,res));
        exception(AuthException.class, (exception, req, res) -> ExceptionHandler.authError(exception, req, res));
        exception(DaoException.class, (exception, req, res) -> ExceptionHandler.daoException(exception, req, res));
        exception(ValidationException.class, (exception, req, res) -> ExceptionHandler.daoException(exception, req, res));
        exception(Exception.class, (exception, req, res) -> ExceptionHandler.internalServerError(exception, req, res));
    }
}
