package Middleware;


import Framework.Exception.Http.AuthException;
import Framework.Init;
import Utlity.Token;
import spark.Request;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by costrategix on 9/28/16.
 */
public class AuthMiddleware{

    String jwtKey,encryptkey;

    public AuthMiddleware(Init boostrap) {
        jwtKey = ((Map<String,Object>)boostrap.config.getSystemConfiguration().get("jwtappkey")).get("value").toString();
        encryptkey = ((Map<String,Object>)boostrap.config.getSystemConfiguration().get("encryptkey")).get("value").toString();
    }

    /**
     * handels the multifacotr token check
     *
     * @param req spark object
     * @throws AuthException throw error
     */
    public  void multiFactorTokenCheck(Request req) throws AuthException {
        String token = req.headers("Authorization");
        Token tokenObj = new Token(jwtKey,encryptkey);
        HashMap<String, Object> tokenObject = tokenObj.parseToken(token);
        if (!tokenObject.get("subject").equals("multifactortoken")) {
            throw new AuthException("Sorry,not a valid  authorization token", "291");
        }
        req.attribute("id", tokenObject.get("id"));

        
    }

    /**
     * handel passwor reset token check
     * @param req spark object
     * @throws AuthException throw error
     */
    public  void passwordResetTokenCheck(Request req) throws AuthException {
        String token = req.headers("Authorization");
        Token tokenObj = new Token(jwtKey,encryptkey);
        HashMap<String, Object> tokenObject = tokenObj.parseToken(token);
       ;
        if ((!tokenObject.get("subject").equals("password-reset")) && (!tokenObject.get("subject").equals("access-token"))) {
            throw new AuthException("Sorry,Not a valid  authorization token", "293");
        }
        req.attribute("id", tokenObject.get("id"));
    }

    /**
     * handle acces token check
     * @param req spark object
     * @throws AuthException throw error
     */

    public  void authTokenCheck(Request req) throws AuthException {
        String token = req.headers("Authorization");
        Token tokenObj = new Token(jwtKey,encryptkey);
        HashMap<String, Object> tokenObject = tokenObj.parseToken(token);
        if (!tokenObject.get("subject").equals("authtoken")) {
            throw new AuthException("Sorry,not a valid  authorization token", "292");
        }
        req.attribute("id", tokenObject.get("id"));
        req.attribute("role", tokenObject.get("role"));
    }
}

