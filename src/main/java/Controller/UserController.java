package Controller;

import Framework.Conf.FrameworkConfiguration;
import Framework.Exception.DaoException;
import Framework.Exception.Http.AuthException;
import Framework.Exception.Http.EmptyPostRequest;
import Framework.Exception.Http.MissingRequiredParameter;
import Framework.Exception.Http.ValidationError;
import Framework.ORM.Adapter.interfaces.AdapterImplementation;
import Framework.ORM.OutputProcessor;
import Utlity.Hash;
import Utlity.Token;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import spark.Request;
import spark.Response;
import utility.OtpGenerator;
import utility.Validation;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.twilio.sdk.resource.instance.UsageCategory.sms;


/**
 * Created by ashrith on 25/1/17.
 */
public class UserController {
    public static FrameworkConfiguration conf;
    public static AdapterImplementation dao;

    int passwordSeedLength = 6;
    int getGenerateTokenOnSignUp =1;
    int requireEmailVerify = 1;
    int requireMobileVerify = 0;
    int mobileOtpLength = 4;
    int emailOtpLength =8;
    OutputProcessor processor;
    String jwtKey = "abc";
    String encryptkey = "abc";
    int multifactorEnabled = 0;
    long accessTokenExpiry = 77760000;
    long multifactorTokenExpiry = 2592000;
    int maxinactivitytime =20;
    int multipleLoginDisabled = 0;
    String issuer ="emysore";
    Utlity.Token tk = null;

    public UserController(Framework.Init bootstrap) {
        this.conf = bootstrap.config;
        this.dao = bootstrap.getDao();
        this.processor = bootstrap.getOutputProcessor();
        passwordSeedLength = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("passwordseedlength")).get("value").toString());
        getGenerateTokenOnSignUp =Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("generateaccesstokenonsignup")).get("value").toString());
        requireEmailVerify = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("requireemailverify")).get("value").toString());
        requireMobileVerify = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("requiremobileverify")).get("value").toString());
        mobileOtpLength  = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("mobileotplength")).get("value").toString());
        emailOtpLength  = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("emailotplength")).get("value").toString());
        jwtKey = ((Map<String,Object>)conf.getSystemConfiguration().get("jwtappkey")).get("value").toString();
        issuer = ((Map<String,Object>)conf.getSystemConfiguration().get("issuer")).get("value").toString();
        encryptkey = ((Map<String,Object>)conf.getSystemConfiguration().get("encryptkey")).get("value").toString();
        accessTokenExpiry = Long.parseLong(((Map<String,Object>)conf.getSystemConfiguration().get("accessTokenExpiry")).get("value").toString());
        multifactorTokenExpiry = Long.parseLong(((Map<String,Object>)conf.getSystemConfiguration().get("multiFactorTokenExpiry")).get("value").toString());
        multifactorEnabled =  Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("enablemultifactorauthentication")).get("value").toString());
        multipleLoginDisabled = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("disablemultiplelogin")).get("value").toString());
        maxinactivitytime = Integer.parseInt(((Map<String,Object>)conf.getSystemConfiguration().get("maxinactivitytime")).get("value").toString());
        tk = new Token(jwtKey,encryptkey);

    }


    public Object register(Request request, Response response) throws Exception {
        String userIdentification = UUID.randomUUID().toString();
        if (request.body().isEmpty() || request.body() == null) {
            throw new EmptyPostRequest("The request body cannot be empty", "300");
        }


        Map<String,Object> params = new Gson().fromJson(request.body(),HashMap.class);
        String[] requiredKeys = {"email", "password", "mobile", "role"};
        Validation validation = new Validation(conf,dao);

        validation.requiredKeyValidations(params,requiredKeys);
        validation.passwordRulesValidation(params.get("password").toString());
        validation.emailValidations(params.get("email").toString());
        validation.mobileValidations(params.get("mobile").toString());


        HashMap<String,Object> data = new HashMap<>();
        for (String paramKey:params.keySet()){
            if (paramKey.equals("password")) {
                params.put(paramKey, Utlity.Hash.hash(params.get(paramKey).toString().trim(),passwordSeedLength));
            }else{
                params.put(paramKey,params.get(paramKey).toString().trim());
            }
            if(!Arrays.asList(requiredKeys).contains(paramKey)){
                throw new ValidationError(paramKey + " is invalid","30");
            }

        }
        params.put("status","active");
        params.put("createdat",Long.toString(System.currentTimeMillis()));

        if(requireEmailVerify == 1){
            params.put("emailverified","0");
            data.put("emailverified", params.get("emailverified"));
            params.put("emailotp",OtpGenerator.random(emailOtpLength));
            params.put("status","inactive");
        }

        if(requireMobileVerify == 1){
            params.put("mobileverified","0");
            data.put("mobileverified", params.get("mobileverified"));
            params.put("mobileotp", OtpGenerator.random(mobileOtpLength));
            params.put("status","inactive");
        }

        if(getGenerateTokenOnSignUp == 1){
            String token = tk.buildToken(userIdentification,issuer,"authtoken",params.get("role").toString(),accessTokenExpiry);
            params.put("logintoken",token);
            params.put("lastlogin",Long.toString(System.currentTimeMillis()));
            data.put("token",token);
        }

        dao.add(params,userIdentification);
        data.put("id",userIdentification);
        data.put("status",params.get("status"));

        if(requireEmailVerify == 1){
            //send mail
        }

        if(requireMobileVerify == 1){
            //send sms
        }



        HashMap<String,Object> retval = new HashMap<>();
        data.put("email", params.get("email"));
        data.put("role", params.get("role"));
        retval.put("status","1");
        retval.put("message","user register successfully");
        retval.put("data",data);
        return new Gson().toJson(retval);

    }

    public Object login(Request request, Response response) throws Exception {
        if (request.body().isEmpty()) {
            throw new EmptyPostRequest("The request body cannot be empty", "300");
        }
        String[] requiredKeys = {"password"};
        Map<String, Object> params = new Gson().fromJson(request.body(), new TypeToken<HashMap<String, Object>>() {
        }.getType());
        Validation validation = new Validation(conf,dao);
        validation.requiredKeyValidations(params, requiredKeys);

        Map<String,Object> userObject = authorize(dao, params);


        if (!userObject.isEmpty()) {
            String rowKey = userObject.get("id").toString();
            String token;
            String message;
            HashMap<String, String> data = new HashMap<>();

            if(multipleLoginDisabled == 1){
                if(userObject.containsKey("logintoken") && !userObject.get("logintoken").equals("")){
                    if(timeCheck(userObject) == true){
                        throw new AuthException("Multiple login is not allowed","999");
                    }
                }
            }

            if (multifactorEnabled == 1) {
                SecureRandom sr = new SecureRandom();
                sr.setSeed(System.currentTimeMillis());
                String loginotp = OtpGenerator.random(mobileOtpLength);
                Map<String, Object> otp = new HashMap<>();
                otp.put("loginotp", loginotp);
                dao.add(otp,rowKey);
                //mail

                //sms

                token = tk.buildToken(rowKey, issuer, "multifactortoken","default", multifactorTokenExpiry);
                message = "continue to second step auth/use token to validate login";

            } else {
                token = tk.buildToken(rowKey, issuer, "authtoken","role", accessTokenExpiry);
                if(multipleLoginDisabled == 1){
                    Map<String,Object> addingnewToken = new HashMap<>();
                    Long startTime = (System.currentTimeMillis()/1000)/60;
                    addingnewToken.put("logintoken",token);
                    addingnewToken.put("lastlogin",startTime.toString());
                    dao.add(addingnewToken,rowKey);
                }
                message = "set token as autorization header";
                String Expiry = tk.parseToken(token).get("expiration").toString();
                data.put("token_expiry", Expiry);

            }
            HashMap<String, Object> result = new HashMap<>();

            data.put("token", token);
            result.put("data", data);
            result.put("message", message);
            result.put("status", "1");
            return new Gson().toJson(result);
        }
        throw new AuthException("oops,Something went wrong", "299");
    }



    public Object requestPasswordResetToken(Request request, Response response) throws MissingRequiredParameter, DaoException, AuthException {
        String id = request.queryParams("loginid");


        if (id == null || id.equals("")) {
            throw new MissingRequiredParameter("The,loginid query param is required", "330");
        }
        String typeof = findTypeof(id);
        List<Map> user = dao.search(typeof,id,1,0);

        if(user.size() <= 0){
            throw new AuthException("invalid user ","24");

        }

        String token = OtpGenerator.random(emailOtpLength);
        Map<String, Object> passwordResetCode = new HashMap<>();
        passwordResetCode.put("passwordresetotp", token);
        dao.add(passwordResetCode,user.get(0).get("key").toString());
        HashMap<String, Object> result = new HashMap<>();
        HashMap<String, String> data = new HashMap<>();
        result.put("status", "1");
        data.put("code", token);
        //mail
        result.put("message", "password rest code is emailed");
        return new Gson().toJson(result);
    }

    public Object resetPassword(Request request, Response response) throws Exception {
        String id = request.queryParams("loginid");
        if (id == null || id.equals("")) {
            throw new EmptyPostRequest("loginid in query params is required", "330");
        }
        if (request.body().isEmpty() || request.body() == null) {
            throw new EmptyPostRequest("params are empty", "300");
        }
        HashMap<String, Object> params = new Gson().fromJson(request.body(), HashMap.class);
        String typeof = findTypeof(id);
        List<Map> user = dao.search(typeof,id,1,0);
        if(user.size() <= 0){
            throw new AuthException("invalid user ","24");

        }
        Map<String,Object> userOject = processor.process(dao.getbyId(user.get(0).get("key").toString(),null,1,0));

        Validation validation = new Validation(conf,dao);
        String[] requiredKeys = {"passwordresetcode", "password"};
        validation.requiredKeyValidations(params, requiredKeys);
        validation.passwordRulesValidation(params.get("password").toString());

        String savePasswordResetToken = userOject.get("passwordresetotp").toString();

        if (!savePasswordResetToken.equals(params.get("passwordresetcode"))) {
            throw new AuthException("The password reset code is invalid or expired ", "212");
        }
        HashMap<String, Object> addToDb = new HashMap<>();
        addToDb.put("passwordresetotp", "");
        addToDb.put("password", Hash.hash(params.get("password").toString(),passwordSeedLength));
        dao.add(addToDb,user.get(0).get("key").toString());
        HashMap<String, String> responseData = new HashMap<>();
        responseData.put("message", "change password successfully");
        responseData.put("status", "1");
        return new Gson().toJson(responseData);
    }

    public Object activateMobile(Request request, Response response) throws MissingRequiredParameter, DaoException, AuthException {
        String id = request.queryParams("loginid");
        String code = request.queryParams("code");

        if (id == null || id.equals("")) {
            throw new MissingRequiredParameter("The,loginid query param is required", "330");
        }

        if (code == null || code.equals("")) {
            throw new MissingRequiredParameter("The,code query param is required", "330");
        }
        String typeof = findTypeof(id);
        List<Map> user = dao.search(typeof,id,1,0);
        if(user.size() <= 0){
            throw new AuthException("invalid user ","24");

        }
        Map<String,Object> userOject = processor.process(dao.getbyId(user.get(0).get("key").toString(),null,1,0));

        if(!userOject.containsKey("mobileotp")){
            throw new AuthException("mobile verification is not required/","24");
        }else{
            if(userOject.containsKey("mobileverified") && userOject.get("mobileverified").toString().equals("1")){
                throw new AuthException("mobile is already verified","24");
            }
        }


        if(userOject.get("mobileotp").toString().equals(code)){
            HashMap<String, Object> responseData = new HashMap<>();
            HashMap<String, Object> updateDb = new HashMap<>();
            if (!userOject.containsKey("emailotp") || userOject.get("email").toString().isEmpty()) {
                updateDb.put("mobileotp", "0");
                updateDb.put("mobileverified", "1");
                updateDb.put("status", "active");
                responseData.put("message", "Account Activated");
                responseData.put("status", "1");

            } else {
                updateDb.put("mobileotp", "");
                updateDb.put("mobileverified", "1");
                responseData.put("status", "1");
                responseData.put("message", "Verify Email to  activate your account");

            }

            dao.add(updateDb,user.get(0).get("key").toString());
            return new Gson().toJson(responseData);
        }

        throw new AuthException("invalid verification code","24");



    }

    public Object activateEmail(Request request, Response response) throws MissingRequiredParameter, AuthException, DaoException {
        String id = request.queryParams("loginid");
        String code = request.queryParams("code");

        if (id == null || id.equals("")) {
            throw new MissingRequiredParameter("The,loginid query param is required", "330");
        }

        if (code == null || code.equals("")) {
            throw new MissingRequiredParameter("The,code query param is required", "330");
        }
        String typeof = findTypeof(id);
        List<Map> user = dao.search(typeof,id,1,0);
        if(user.size() <= 0){
            throw new AuthException("invalid user ","24");

        }
        Map<String,Object> userOject = processor.process(dao.getbyId(user.get(0).get("key").toString(),null,1,0));

        if(!userOject.containsKey("emailotp")){
            throw new AuthException("email verification is not required","24");
        }else{
            if(userOject.containsKey("emailverified") && userOject.get("emailverified").toString().equals("1")){
                throw new AuthException("email is already verified","24");
            }
        }

        if(userOject.get("emailotp").toString().equals(code)){
            HashMap<String, Object> responseData = new HashMap<>();
            HashMap<String, Object> updateDb = new HashMap<>();
            if (!userOject.containsKey("mobileotp") || userOject.get("mobileotp").toString().isEmpty()) {
                updateDb.put("emailotp", "");
                updateDb.put("emailverified", "1");
                updateDb.put("status", "active");
                responseData.put("message", "Account Activated");
                responseData.put("status", "1");

            } else {
                updateDb.put("emailotp", "");
                updateDb.put("emailverified", "1");
                responseData.put("status", "1");
                responseData.put("message", "Verify mobile to  activate your account");

            }

            dao.add(updateDb,user.get(0).get("key").toString());
            return new Gson().toJson(responseData);
        }

        throw new AuthException("invalid verification code","24");

    }

    private Map<String,Object> authorize(AdapterImplementation dao,Map<String,Object> inParms) throws Exception {
        String id = "";
        if (inParms.containsKey("loginid")) {
            id = inParms.get("loginid").toString();
        }

        if (id.isEmpty()) {
            throw new MissingRequiredParameter("Either mobile number or email is required", "330");
        }

        String typeOf = findTypeof(id);

        if (typeOf.equals("error")) {
            throw new MissingRequiredParameter("invalid mobile/email", "310");

        }
        List<Map> user = dao.search(typeOf,id,1,0);


        if(user.size() <= 0){
            throw new AuthException("user id does not exist", "297");
        }
        Map<String,Object> userOject = processor.process(dao.getbyId(user.get(0).get("key").toString(),null,1,0));

        if(Hash.compareHash(userOject.get("password").toString(),inParms.get("password").toString(),passwordSeedLength)){
            if(!userOject.get("status").toString().equals("active")){
                throw new AuthException("user is " + userOject.get("status").toString(), "297");
            }
        }else{
            throw new AuthException("invalid credentials", "297");
        }
        userOject.put("id",user.get(0).get("key"));
        return userOject;
    }

    private String findTypeof(String value) {
        Pattern emailPattern = Pattern.compile("^(.+)@(.+)$", Pattern.CASE_INSENSITIVE);
        Pattern mobilePattern = Pattern.compile("^\\+[1-9]{1}[0-9]{3,14}$");
        Matcher email = emailPattern.matcher(value);
        Matcher mobile = mobilePattern.matcher(value);
        if (email.matches()) {
            return "email";
        }
        if (mobile.matches()) {
            return "mobile";
        }
        return "error";
    }

    private boolean timeCheck(Map<String, Object> userObject) {
        Boolean status = true;
        Long currentTime = (System.currentTimeMillis()/1000)/60;
        Long lastAccessTime = Long.parseLong(userObject.get("lastlogin").toString());
        if((currentTime - lastAccessTime) > maxinactivitytime){
            status = false;
        }

        return status;
    }

    public Object steptwologin(Request request, Response response) throws Exception {
        if (request.body().isEmpty()) {
            throw new EmptyPostRequest("The request body cannot be empty", "300");
        }

        String[] requiredKeys = {"code"};
        Map<String, Object> params = new Gson().fromJson(request.body(), new TypeToken<HashMap<String, String>>() {
        }.getType());
        Validation validation = new Validation(conf,dao);
        validation.requiredKeyValidations(params, requiredKeys);
        String id = request.attribute("id").toString();
        Map<String,Object> userObject = processor.process(dao.getbyId(id,null,1,0));
        if (userObject.containsKey("loginotp") && userObject.get("loginotp").toString().equals(params.get("code"))) {
            HashMap<String, Object> result = new HashMap<>();
            result.put("status", "1");
            HashMap<String, String> token = new HashMap<>();
            token.put("token", tk.buildToken(id, issuer, "authtoken",userObject.get("role").toString(), accessTokenExpiry));
            String Expiry = tk.parseToken(token.get("token")).get("expiration").toString();
            token.put("token_expiry", Expiry);
            Map<String,Object> addingnewToken = new HashMap<>();
            addingnewToken.put("logintoken",token.get("token"));
            Long startTime = (System.currentTimeMillis()/1000)/60;
            addingnewToken.put("lastlogin",startTime.toString());
            addingnewToken.put("loginotp","");
            dao.add(addingnewToken,id);
            result.put("data", token);
            result.put("message", "Access granted,please replace the authorization header with,new token");
            return new Gson().toJson(result);
        }
        throw new AuthException("Invalid verification code,please try again", "214");
    }

    public Object getlogedinuser(Request request, Response response) throws DaoException {
        String id= request.attribute("id");
        Map<String,Object> userObject = processor.process(dao.getbyId(id,null,1,0));
        HashMap<String, Object> result = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();
        data.put("email",userObject.get("email"));
        data.put("mobile",userObject.get("mobile"));
        data.put("role",userObject.get("role"));
        data.put("id",id);
        result.put("status","1");
        result.put("data",data);
        result.put("message","successfully retrived");
        return new Gson().toJson(result);


    }

    public Object getUserByloginId(Request request, Response response) throws AuthException, DaoException {
        String loginid = request.params("loginid");
        if(loginid == null || loginid.isEmpty()){
            throw new AuthException("login id is required","23");
        }

        String typeof = findTypeof(loginid);
        List<Map> user = dao.search(typeof,loginid,1,0);
        if(user.size() <= 0){
            throw new AuthException("invalid user ","24");

        }
        Map<String,Object> userObject = processor.process(dao.getbyId(user.get(0).get("key").toString(),null,1,0));
        HashMap<String, Object> result = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();
        data.put("email",userObject.get("email"));
        data.put("mobile",userObject.get("mobile"));
        data.put("role",userObject.get("role"));
        data.put("id",user.get(0).get("key"));
        result.put("status","1");
        result.put("data",data);
        result.put("message","successfully retrived");
        return new Gson().toJson(result);
    }

    public Object tokenCheck(Request request, Response response) {
        String id = request.attribute("id").toString();
        HashMap<String, Object> responseData = new HashMap<String, Object>();
        responseData.put("status", "1");
        responseData.put("message", "Valid Auth token");
        HashMap<String, String> data = new HashMap<>();
        data.put("userid", id);
        responseData.put("data", data);
        return new Gson().toJson(responseData);
    }

    public Object renewToken(Request request, Response response) throws DaoException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException {
        String id = request.attribute("id").toString();
        Map<String,Object> user = processor.process(dao.getbyId(id,null,1,0));
        Token tokenObj = new Token(jwtKey,encryptkey);
        HashMap<String, Object> responseData = new HashMap<String, Object>();
        String token = tokenObj.buildToken(id, issuer, "authtoken",user.get("role").toString(),accessTokenExpiry);
        HashMap<String, String> data = new HashMap<>();
        data.put("token", token);
        responseData.put("status", "0");
        responseData.put("message", "Please replace this token with new Token");
        responseData.put("data", data);
        return new Gson().toJson(responseData);
    }
}
